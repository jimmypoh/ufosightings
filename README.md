## Description
The National UFO Research Center (NUFORC) collects and serves over 100,000 reports of UFO sightings.
This dataset contains the report content itself including the time, location duration, and other attributes in both the raw form as it is recorded on the NUFORC site as well as a refined, standardized form that also contains lat/lon coordinates.

The dataset is obtained from https://data.world/timothyrenner/ufo-sightings

## Result 
![Screenshot](folium_result.PNG)

